"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by calem on 11/10/2016.
 */
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var login_page_component_1 = require("./components/login-page/login-page.component");
var home_page_component_1 = require("./components/home-page/home-page.component");
var profile_page_component_1 = require("./components/profile-page/profile-page.component");
var search_result_component_1 = require("./components/search-result/search-result.component");
var problem_page_component_1 = require("./components/problem-page/problem-page.component");
var routes = [
    {
        path: '',
        redirectTo: '/login',
        pathMatch: 'full'
    },
    {
        path: 'login',
        component: login_page_component_1.LoginPageComponent
    },
    {
        path: 'home',
        component: home_page_component_1.HomePageComponent
    },
    {
        path: 'search',
        component: search_result_component_1.SearchResultComponent
    },
    {
        path: 'profile/:id',
        component: profile_page_component_1.ProfilePageComponent
    },
    {
        path: 'problem/:id',
        component: problem_page_component_1.ProblemPageComponent
    }
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    return AppRoutingModule;
}());
AppRoutingModule = __decorate([
    core_1.NgModule({
        imports: [
            router_1.RouterModule.forRoot(routes)
        ],
        exports: [
            router_1.RouterModule
        ]
    }),
    __metadata("design:paramtypes", [])
], AppRoutingModule);
exports.AppRoutingModule = AppRoutingModule;
//# sourceMappingURL=app-routing.module.js.map