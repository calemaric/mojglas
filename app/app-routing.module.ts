/**
 * Created by calem on 11/10/2016.
 */
import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {LoggedInGuard} from "./services/LoggedInGuard";
import {AppComponent} from "./components/app.component";
import {LoginPageComponent} from "./components/login-page/login-page.component";
import {HomePageComponent} from "./components/home-page/home-page.component";
import {ProfilePageComponent} from "./components/profile-page/profile-page.component";
import {SearchResultComponent} from "./components/search-result/search-result.component";
import {ProblemPageComponent} from "./components/problem-page/problem-page.component";

const routes: Routes = [
    {
        path: '',
        redirectTo: '/login',
        pathMatch: 'full'
    },
    {
        path: 'login',
        component: LoginPageComponent
    },
    {
        path: 'home',
        component: HomePageComponent
    },
    {
        path: 'search',
        component: SearchResultComponent
    },
    {
        path: 'profile/:id',
        component: ProfilePageComponent
    },
    {
        path: 'problem/:id',
        component: ProblemPageComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})

export class AppRoutingModule{}