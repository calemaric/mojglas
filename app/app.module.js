"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by calem on 11/9/2016.
 */
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require("@angular/forms");
var app_routing_module_1 = require("./app-routing.module");
var http_1 = require("@angular/http");
var LoggedInGuard_1 = require("./services/LoggedInGuard");
var auth_service_1 = require("./services/auth.service");
var app_component_1 = require("./components/app.component");
var login_page_component_1 = require("./components/login-page/login-page.component");
var register_form_component_1 = require("./components/login-page/register-form/register-form.component");
var home_page_component_1 = require("./components/home-page/home-page.component");
var profile_page_component_1 = require("./components/profile-page/profile-page.component");
var search_result_component_1 = require("./components/search-result/search-result.component");
var problem_item_component_1 = require("./components/problem-item/problem-item.component");
var problem_page_component_1 = require("./components/problem-page/problem-page.component");
var selected_problem_component_1 = require("./components/selected-problem/selected-problem.component");
var search_problem_component_1 = require("./components/search-problem/search-problem.component");
var recomendation_component_1 = require("./components/recomendation.component/recomendation.component");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_browser_1.BrowserModule,
            forms_1.FormsModule,
            http_1.HttpModule,
            app_routing_module_1.AppRoutingModule
        ],
        declarations: [
            app_component_1.AppComponent,
            login_page_component_1.LoginPageComponent,
            register_form_component_1.RegisterFormComponent,
            home_page_component_1.HomePageComponent,
            problem_item_component_1.ProblemItemComponent,
            profile_page_component_1.ProfilePageComponent,
            search_result_component_1.SearchResultComponent,
            problem_page_component_1.ProblemPageComponent,
            selected_problem_component_1.SelectedProblemComponent,
            search_problem_component_1.SearchProblemComponent,
            recomendation_component_1.RecomendationComponent
        ],
        bootstrap: [
            app_component_1.AppComponent
        ],
        providers: [
            auth_service_1.AuthService,
            LoggedInGuard_1.LoggedInGuard
        ]
    }),
    __metadata("design:paramtypes", [])
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map