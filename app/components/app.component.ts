import {Component, HostListener, OnInit} from "@angular/core";
import {Route, ActivatedRoute, Router} from "@angular/router";
/**
 * Created by calem on 11/12/2016.
 */

@Component({
    moduleId: module.id,
    selector: 'my-app',
    template: `
  <!--========== HEADER  ==========-->
    <header *ngIf="currentRoute != '/login' && currentRoute != '/'" class="header-center-aligned-transparent navbar-fixed-top header-sticky" [ngClass]="{'header-animate': animateHeader}">

        <!-- Navbar -->
        <nav class="navbar mega-menu" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="menu-container">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="toggle-icon"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- Logo -->
                    <div class="navbar-logo">
                        <a class="navbar-logo-wrap" routerLink="/home">
                            <img class="navbar-logo-img navbar-logo-img-white" src="img/logo-white.png" alt="Ark"  *ngIf="!animateHeader">
                            <img class="navbar-logo-img navbar-logo-img-dark" src="img/logo.png" alt="Ark" *ngIf="animateHeader">
                        </a>
                    </div>
                    <!-- End Logo -->
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse nav-collapse">
                    <div class="menu-container">

                        <ul class="nav navbar-nav navbar-nav-left">
                            <li class="nav-item dropdown">
                            <a routerLink="/home" class="nav-item-child dropdown-toggle radius-3 active" >Početna</a>
                            </li>
                            <li class="nav-item dropdown"><a routerLink="/search" class="nav-item-child dropdown-toggle radius-3" [ngClass]="{'black': animateHeader}">Pretraga</a></li>
                            <li class="nav-item"><a routerLink='/profile/1' class="nav-item-child dropdown-toggle radius-3" [ngClass]="{'black': animateHeader}">Poslanici</a>
                            </li>
                        </ul>

                        <ul class="nav navbar-nav navbar-nav-right">
                            <li class="nav-item dropdown"><a class="nav-item-child dropdown-toggle radius-3" [ngClass]="{'black': animateHeader}">Profil</a></li>
                            <li class="nav-item dropdown"><a class="nav-item-child dropdown-toggle radius-3" [ngClass]="{'black': animateHeader}">Kontakt</a>
                            </li>
                            <li class="nav-item nav-item-bg form-modal-nav"><a
                                    class="nav-item-child form-modal-login radius-3" style="cursor: pointer" (click)="logout()" id="login">Logout</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- End Navbar Collapse -->
            </div>
            <!--// End Container-->
        </nav>
        <!-- Navbar -->
    </header>
    <!--========== END HEADER ==========-->



        <router-outlet></router-outlet>
    `,
    styleUrls: [
        'app.component.css'
    ],
    providers: [
        Location
    ]
})

export class AppComponent implements OnInit{
    private animateHeader: boolean;
    private currentRoute: string;

    constructor(
        private router: Router
    ) {
    }

    @HostListener('window:scroll', ['$event'])
    doSomething(event) {
        if(this.currentRoute == '/home') {
            this.animateHeader = document.body.scrollTop > 100;
        } else {
            this.animateHeader = true;
        }
    }

    ngOnInit(): void {
        this.router.events.subscribe((val) => {
            this.currentRoute = val.url;

            if(this.currentRoute != '/home') {
                this.animateHeader = true;
            }
            })
    }

    logout(): void {
        localStorage.clear();
        this.router.navigate(['/login']);
    }
}