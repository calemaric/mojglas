"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
/**
 * Created by calem on 11/12/2016.
 */
var AppComponent = (function () {
    function AppComponent(router) {
        this.router = router;
    }
    AppComponent.prototype.doSomething = function (event) {
        if (this.currentRoute == '/home') {
            this.animateHeader = document.body.scrollTop > 100;
        }
        else {
            this.animateHeader = true;
        }
    };
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.router.events.subscribe(function (val) {
            _this.currentRoute = val.url;
            if (_this.currentRoute != '/home') {
                _this.animateHeader = true;
            }
        });
    };
    AppComponent.prototype.logout = function () {
        localStorage.clear();
        this.router.navigate(['/login']);
    };
    return AppComponent;
}());
__decorate([
    core_1.HostListener('window:scroll', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], AppComponent.prototype, "doSomething", null);
AppComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'my-app',
        template: "\n  <!--========== HEADER  ==========-->\n    <header *ngIf=\"currentRoute != '/login' && currentRoute != '/'\" class=\"header-center-aligned-transparent navbar-fixed-top header-sticky\" [ngClass]=\"{'header-animate': animateHeader}\">\n\n        <!-- Navbar -->\n        <nav class=\"navbar mega-menu\" role=\"navigation\">\n            <div class=\"container\">\n                <!-- Brand and toggle get grouped for better mobile display -->\n                <div class=\"menu-container\">\n                    <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\".nav-collapse\">\n                        <span class=\"sr-only\">Toggle navigation</span>\n                        <span class=\"toggle-icon\"></span>\n                        <span class=\"icon-bar\"></span>\n                        <span class=\"icon-bar\"></span>\n                    </button>\n                    <!-- Logo -->\n                    <div class=\"navbar-logo\">\n                        <a class=\"navbar-logo-wrap\" routerLink=\"/home\">\n                            <img class=\"navbar-logo-img navbar-logo-img-white\" src=\"img/logo-white.png\" alt=\"Ark\"  *ngIf=\"!animateHeader\">\n                            <img class=\"navbar-logo-img navbar-logo-img-dark\" src=\"img/logo.png\" alt=\"Ark\" *ngIf=\"animateHeader\">\n                        </a>\n                    </div>\n                    <!-- End Logo -->\n                </div>\n\n                <!-- Collect the nav links, forms, and other content for toggling -->\n                <div class=\"collapse navbar-collapse nav-collapse\">\n                    <div class=\"menu-container\">\n\n                        <ul class=\"nav navbar-nav navbar-nav-left\">\n                            <li class=\"nav-item dropdown\">\n                            <a routerLink=\"/home\" class=\"nav-item-child dropdown-toggle radius-3 active\" >Po\u010Detna</a>\n                            </li>\n                            <li class=\"nav-item dropdown\"><a routerLink=\"/search\" class=\"nav-item-child dropdown-toggle radius-3\" [ngClass]=\"{'black': animateHeader}\">Pretraga</a></li>\n                            <li class=\"nav-item\"><a routerLink='/profile/1' class=\"nav-item-child dropdown-toggle radius-3\" [ngClass]=\"{'black': animateHeader}\">Poslanici</a>\n                            </li>\n                        </ul>\n\n                        <ul class=\"nav navbar-nav navbar-nav-right\">\n                            <li class=\"nav-item dropdown\"><a class=\"nav-item-child dropdown-toggle radius-3\" [ngClass]=\"{'black': animateHeader}\">Profil</a></li>\n                            <li class=\"nav-item dropdown\"><a class=\"nav-item-child dropdown-toggle radius-3\" [ngClass]=\"{'black': animateHeader}\">Kontakt</a>\n                            </li>\n                            <li class=\"nav-item nav-item-bg form-modal-nav\"><a\n                                    class=\"nav-item-child form-modal-login radius-3\" style=\"cursor: pointer\" (click)=\"logout()\" id=\"login\">Logout</a>\n                            </li>\n                        </ul>\n                    </div>\n                </div>\n                <!-- End Navbar Collapse -->\n            </div>\n            <!--// End Container-->\n        </nav>\n        <!-- Navbar -->\n    </header>\n    <!--========== END HEADER ==========-->\n\n\n\n        <router-outlet></router-outlet>\n    ",
        styleUrls: [
            'app.component.css'
        ],
        providers: [
            Location
        ]
    }),
    __metadata("design:paramtypes", [router_1.Router])
], AppComponent);
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map