"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var user_service_1 = require("../../services/user.service");
var router_1 = require("@angular/router");
var problem_service_1 = require("../../services/problem.service");
var Filter_1 = require("../../model/Filter");
var auth_service_1 = require("../../services/auth.service");
var moment = require("moment/moment");
/**
 * Created by calem on 11/12/2016.
 */
var HomePageComponent = (function () {
    function HomePageComponent(userService, router, problemService, auth) {
        this.userService = userService;
        this.router = router;
        this.problemService = problemService;
        this.auth = auth;
        this.problemList = [];
        this.newProblem = {
            title: '',
            descirption: '',
            topic: 5,
            date: moment().format('YYYY-MM-DD'),
            user: this.auth.getUserInfo().id,
        };
    }
    HomePageComponent.prototype.getUserInfo = function () {
        this.userService.getIndividualUser(2)
            .then(function (res) {
        });
    };
    HomePageComponent.prototype.ngOnInit = function () {
        var _this = this;
        var filter = new Filter_1.Filter();
        filter.order_by = 'votes_up';
        filter.number_of = 3;
        this.problemService.getProblems(filter.deserialize())
            .then(function (res) {
            _this.problemList = res.slice(0, 3);
        });
    };
    HomePageComponent.prototype.gotoSearch = function (id) {
        var navigationExtras = {
            queryParams: {},
            fragment: ''
        };
        if (id) {
            navigationExtras.queryParams['filter'] = id;
        }
        if (this.searchQuery) {
            navigationExtras.queryParams['search-query'] = this.searchQuery;
        }
        this.router.navigate(['/search'], navigationExtras);
    };
    HomePageComponent.prototype.onNewProblem = function (event) {
        event.preventDefault();
        this.problemService.newProblem(this.newProblem)
            .then(function (res) {
            console.log(res);
        });
    };
    return HomePageComponent;
}());
HomePageComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'home-page',
        templateUrl: 'home-page.component.html',
        styleUrls: [
            'home-page.component.css'
        ],
        providers: [
            user_service_1.UserService,
            problem_service_1.ProblemService
        ]
    }),
    __metadata("design:paramtypes", [user_service_1.UserService,
        router_1.Router,
        problem_service_1.ProblemService,
        auth_service_1.AuthService])
], HomePageComponent);
exports.HomePageComponent = HomePageComponent;
//# sourceMappingURL=home-page.component.js.map