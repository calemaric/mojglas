import {Component, HostListener, OnInit} from "@angular/core";
import {UserService} from "../../services/user.service";
import {Problem} from "../../model/Problem";
import {User} from "../../model/User";
import {Router, NavigationExtras} from "@angular/router";
import {ProblemService} from "../../services/problem.service";
import {Filter} from "../../model/Filter";
import {AuthService} from "../../services/auth.service";
import * as moment from 'moment/moment';
/**
 * Created by calem on 11/12/2016.
 */

@Component({
    moduleId: module.id,
    selector: 'home-page',
    templateUrl: 'home-page.component.html',
    styleUrls: [
        'home-page.component.css'
    ],
    providers: [
        UserService,
        ProblemService
    ]
})

export class HomePageComponent implements OnInit{
    private problemList : Problem[] = [];
    private searchQuery: string;

    private newProblem: any = {
        title: '',
        descirption: '',
        topic: 5,
        date: moment().format('YYYY-MM-DD'),
        user: this.auth.getUserInfo().id,
    }
    constructor(
        private userService: UserService,
        private router: Router,
        private problemService: ProblemService,
        private auth: AuthService
    ) {}

    getUserInfo(): void {
        this.userService.getIndividualUser(2)
            .then((res) => {
            })
    }

    ngOnInit(): void {
        let filter = new Filter();
        filter.order_by = 'votes_up';
        filter.number_of = 3;
        this.problemService.getProblems(filter.deserialize())
            .then((res) => {
                this.problemList = res.slice(0,3);
            })
    }

    gotoSearch(id: number): void {
        let navigationExtras = {
            queryParams: {

            },
            fragment: ''
        };

        if(id) {
            navigationExtras.queryParams['filter'] = id;
        }

        if(this.searchQuery) {
            navigationExtras.queryParams['search-query'] = this.searchQuery;
        }

        this.router.navigate(['/search'], navigationExtras);
    }

    onNewProblem(event): void {
        event.preventDefault();
        this.problemService.newProblem(this.newProblem)
            .then((res) => {
                console.log(res);
            });
    }


}