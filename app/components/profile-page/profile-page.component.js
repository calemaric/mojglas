"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var auth_service_1 = require("../../services/auth.service");
var Problem_1 = require("../../model/Problem");
var problem_service_1 = require("../../services/problem.service");
var User_1 = require("../../model/User");
var user_service_1 = require("../../services/user.service");
/**
 * Created by calem on 11/12/2016.
 */
var ProfilePageComponent = (function () {
    function ProfilePageComponent(router, auth, problemService, userService) {
        this.router = router;
        this.auth = auth;
        this.problemService = problemService;
        this.userService = userService;
        this.user = new User_1.User();
        this.bla = new Problem_1.Problem();
    }
    ProfilePageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.router.params.subscribe(function (params) {
            _this.user.id = +params['id'];
            _this.userService.getIndividualUser(_this.user.id)
                .then(function (res) {
                _this.user = res;
                _this.imgSrc = _this.user.image;
            })
                .catch(function (res) {
                console.log(res);
            });
        });
    };
    ProfilePageComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    return ProfilePageComponent;
}());
ProfilePageComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'profile-page',
        templateUrl: 'profile-page.component.html',
        styleUrls: [
            'profile-page.component.css'
        ],
        providers: [
            user_service_1.UserService,
            problem_service_1.ProblemService
        ]
    }),
    __metadata("design:paramtypes", [router_1.ActivatedRoute,
        auth_service_1.AuthService,
        problem_service_1.ProblemService,
        user_service_1.UserService])
], ProfilePageComponent);
exports.ProfilePageComponent = ProfilePageComponent;
//# sourceMappingURL=profile-page.component.js.map