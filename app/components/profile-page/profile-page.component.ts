import {Component, OnInit} from "@angular/core";
import {Observable, Subscription} from "rxjs";
import {Router, ActivatedRoute} from "@angular/router";
import {AuthService} from "../../services/auth.service";
import {Problem} from "../../model/Problem";
import {ProblemService} from "../../services/problem.service";
import {Subscribable} from "rxjs/Observable";
import {User} from "../../model/User";
import {UserService} from "../../services/user.service";
/**
 * Created by calem on 11/12/2016.
 */
@Component({
    moduleId: module.id,
    selector: 'profile-page',
    templateUrl: 'profile-page.component.html',
    styleUrls: [
        'profile-page.component.css'
    ],
    providers: [
        UserService,
        ProblemService
    ]
})

export class ProfilePageComponent{
    private sub: Subscription;
    private user: User = new User();
    private imgSrc: string;

    constructor(
        private router: ActivatedRoute,
        private auth: AuthService,
        private problemService: ProblemService,
        private userService: UserService

    ){}

    ngOnInit(): void {
        this.sub = this.router.params.subscribe(params => {
            this.user.id = +params['id'];

            this.userService.getIndividualUser(this.user.id)
                .then((res) => {
                    this.user = res;
                    this.imgSrc = this.user.image;
                })
                .catch((res) => {
                    console.log(res);
                })
        });
    }

    ngOnDestroy(): void {
        this.sub.unsubscribe();
    }

    private bla: Problem = new Problem();
}