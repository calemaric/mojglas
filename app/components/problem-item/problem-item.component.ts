import {Component, Input, OnInit} from "@angular/core";
import {Problem} from "../../model/Problem";
import {Router} from "@angular/router";
/**
 * Created by calem on 11/12/2016.
 */

@Component({
    moduleId: module.id,
    selector: 'problem-item',
    templateUrl: 'problem-item.component.html',
    styleUrls: [
        'problem-item.component.css'
    ]
})

export class ProblemItemComponent implements OnInit{
    @Input() item : Problem;

    constructor(
        private router: Router
    ) {}

    ngOnInit(): void {
    }

    onProblemClicked(): void {
        this.router.navigate(['/problem', this.item.id]);
    }

}