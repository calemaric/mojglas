"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var Problem_1 = require("../../model/Problem");
var router_1 = require("@angular/router");
/**
 * Created by calem on 11/12/2016.
 */
var ProblemItemComponent = (function () {
    function ProblemItemComponent(router) {
        this.router = router;
    }
    ProblemItemComponent.prototype.ngOnInit = function () {
    };
    ProblemItemComponent.prototype.onProblemClicked = function () {
        this.router.navigate(['/problem', this.item.id]);
    };
    return ProblemItemComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Problem_1.Problem)
], ProblemItemComponent.prototype, "item", void 0);
ProblemItemComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'problem-item',
        templateUrl: 'problem-item.component.html',
        styleUrls: [
            'problem-item.component.css'
        ]
    }),
    __metadata("design:paramtypes", [router_1.Router])
], ProblemItemComponent);
exports.ProblemItemComponent = ProblemItemComponent;
//# sourceMappingURL=problem-item.component.js.map