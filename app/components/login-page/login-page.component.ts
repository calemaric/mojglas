import {Component, OnInit} from "@angular/core";
import {AuthService} from "../../services/auth.service";
import {Router} from "@angular/router";

@Component({
    moduleId: module.id,
    selector: 'login-page',
    templateUrl: 'login-page.component.html',
    styleUrls: [
        'login-page.component.css'
    ]
})

export class LoginPageComponent implements OnInit{
    private username: string;
    private password: string;

    constructor(
        private auth: AuthService,
        private router: Router
    ) {}

    ngOnInit(): void {
        if(this.auth.isLoggedIn()) {
            let creds = {
                username: localStorage.getItem('username'),
                password: localStorage.getItem('password')
            };

            if(creds.username) {
                this.auth.login(creds).then((res) => {
                    this.router.navigate(['/home']);
                })
                    .catch((res) => {
                        console.log(res);
                    })
            }
        }
    }

    onLoginClicked(event): void {
        event.preventDefault();
        let userInfo = {
            username: this.username,
            password: this.password
        };

        console.log("login clicked");

        this.auth.login(userInfo)
            .then((res) => {
                console.log(res);

                this.router.navigate(['/home']);

                //t
            })
            .catch((err) => {
                console.log(err);
            });

    }
}