import {Component} from "@angular/core";
import {AuthService} from "../../../services/auth.service";
import {Router} from "@angular/router";
/**
 * Created by calem on 11/12/2016.
 */

@Component({
    moduleId: module.id,
    selector: 'register-form',
    templateUrl: 'register-form.component.html',
    styleUrls: [
        'register-form.component.css'
    ],
    providers: [
        AuthService
    ]
})


export class RegisterFormComponent {
    private query: any = {};

    constructor(
        private authService: AuthService,
        private router: Router
    ) {}

    onRegister(query: any,event): void {
        event.preventDefault();
        this.authService.register(query)
            .then((res) => {
                console.log('bla');
            })
    }

}