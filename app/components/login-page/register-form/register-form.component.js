"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var auth_service_1 = require("../../../services/auth.service");
var router_1 = require("@angular/router");
/**
 * Created by calem on 11/12/2016.
 */
var RegisterFormComponent = (function () {
    function RegisterFormComponent(authService, router) {
        this.authService = authService;
        this.router = router;
        this.query = {};
    }
    RegisterFormComponent.prototype.onRegister = function (query, event) {
        event.preventDefault();
        this.authService.register(query)
            .then(function (res) {
            console.log('bla');
        });
    };
    return RegisterFormComponent;
}());
RegisterFormComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'register-form',
        templateUrl: 'register-form.component.html',
        styleUrls: [
            'register-form.component.css'
        ],
        providers: [
            auth_service_1.AuthService
        ]
    }),
    __metadata("design:paramtypes", [auth_service_1.AuthService,
        router_1.Router])
], RegisterFormComponent);
exports.RegisterFormComponent = RegisterFormComponent;
//# sourceMappingURL=register-form.component.js.map