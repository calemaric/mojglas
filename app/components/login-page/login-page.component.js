"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var auth_service_1 = require("../../services/auth.service");
var router_1 = require("@angular/router");
var LoginPageComponent = (function () {
    function LoginPageComponent(auth, router) {
        this.auth = auth;
        this.router = router;
    }
    LoginPageComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.auth.isLoggedIn()) {
            var creds = {
                username: localStorage.getItem('username'),
                password: localStorage.getItem('password')
            };
            if (creds.username) {
                this.auth.login(creds).then(function (res) {
                    _this.router.navigate(['/home']);
                })
                    .catch(function (res) {
                    console.log(res);
                });
            }
        }
    };
    LoginPageComponent.prototype.onLoginClicked = function (event) {
        var _this = this;
        event.preventDefault();
        var userInfo = {
            username: this.username,
            password: this.password
        };
        console.log("login clicked");
        this.auth.login(userInfo)
            .then(function (res) {
            console.log(res);
            _this.router.navigate(['/home']);
            //t
        })
            .catch(function (err) {
            console.log(err);
        });
    };
    return LoginPageComponent;
}());
LoginPageComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'login-page',
        templateUrl: 'login-page.component.html',
        styleUrls: [
            'login-page.component.css'
        ]
    }),
    __metadata("design:paramtypes", [auth_service_1.AuthService,
        router_1.Router])
], LoginPageComponent);
exports.LoginPageComponent = LoginPageComponent;
//# sourceMappingURL=login-page.component.js.map