"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var Problem_1 = require("../../model/Problem");
var problem_service_1 = require("../../services/problem.service");
var router_1 = require("@angular/router");
var auth_service_1 = require("../../services/auth.service");
/**
 * Created by calem on 11/12/2016.
 */
var SearchProblemComponent = (function () {
    function SearchProblemComponent(problemService, router, auth) {
        this.problemService = problemService;
        this.router = router;
        this.auth = auth;
        this.hover = false;
    }
    SearchProblemComponent.prototype.ngOnInit = function () {
        this.user = this.auth.getUserInfo();
    };
    SearchProblemComponent.prototype.onMouseEnter = function () {
        this.hover = true;
    };
    SearchProblemComponent.prototype.onMouseLeave = function () {
        this.hover = false;
    };
    SearchProblemComponent.prototype.onSelectedClicked = function () {
        this.router.navigate(['/problem', this.problem.id]);
    };
    SearchProblemComponent.prototype.voteUp = function () {
        var _this = this;
        var self = this;
        var vote = {
            'user_id': self.user.id,
            'problem_id': self.problem.id,
            'vote': 1
        };
        this.problemService.newProblemVote(vote)
            .then(function (res) {
            _this.problem.votes_up++;
        });
    };
    return SearchProblemComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Problem_1.Problem)
], SearchProblemComponent.prototype, "problem", void 0);
SearchProblemComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'search-problem',
        templateUrl: 'search-problem.component.html',
        styleUrls: [
            'search-problem.component.css'
        ],
        providers: [
            problem_service_1.ProblemService
        ]
    }),
    __metadata("design:paramtypes", [problem_service_1.ProblemService,
        router_1.Router,
        auth_service_1.AuthService])
], SearchProblemComponent);
exports.SearchProblemComponent = SearchProblemComponent;
//# sourceMappingURL=search-problem.component.js.map