"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var problem_service_1 = require("../../services/problem.service");
var router_1 = require("@angular/router");
var auth_service_1 = require("../../services/auth.service");
/**
 * Created by calem on 11/12/2016.
 */
var ProblemPageComponent = (function () {
    function ProblemPageComponent(router, auth, problemService) {
        this.router = router;
        this.auth = auth;
        this.problemService = problemService;
        this.problem = null;
        this.imgSrc = '';
    }
    ProblemPageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.router.params.subscribe(function (params) {
            _this.problemService.getProblem(params['id'])
                .then(function (res) {
                _this.problem = res;
                _this.imgSrc = _this.problem.image;
                _this.getRecomendation();
            })
                .catch(function (res) {
                console.log(res);
            });
        });
    };
    ProblemPageComponent.prototype.getRecomendation = function () {
        var _this = this;
        this.problemService.getRecomendation(this.problem.topic.id)
            .then(function (res) {
            _this.recomendation = res;
        });
    };
    ProblemPageComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    return ProblemPageComponent;
}());
ProblemPageComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'problem-page',
        templateUrl: 'problem-page.component.html',
        styleUrls: [
            'problem-page.component.css'
        ],
        providers: [
            problem_service_1.ProblemService
        ]
    }),
    __metadata("design:paramtypes", [router_1.ActivatedRoute,
        auth_service_1.AuthService,
        problem_service_1.ProblemService])
], ProblemPageComponent);
exports.ProblemPageComponent = ProblemPageComponent;
//# sourceMappingURL=problem-page.component.js.map