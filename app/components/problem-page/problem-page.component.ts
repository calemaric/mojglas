import {Component, OnInit} from "@angular/core";
import {ProblemService} from "../../services/problem.service";
import {Route, ActivatedRoute} from "@angular/router";
import {Problem} from "../../model/Problem";
import {Subscription} from "rxjs";
import {AuthService} from "../../services/auth.service";
import {User} from "../../model/User";
/**
 * Created by calem on 11/12/2016.
 */

@Component({
    moduleId: module.id,
    selector: 'problem-page',
    templateUrl: 'problem-page.component.html',
    styleUrls: [
        'problem-page.component.css'
    ],
    providers: [
        ProblemService
    ]
})

export class ProblemPageComponent implements OnInit{
    private sub: Subscription;
    private problem: Problem = null;
    private imgSrc: string = '';
    private recomendation: User [];

    constructor(
        private router: ActivatedRoute,
        private auth: AuthService,
        private problemService: ProblemService

    ){}

    ngOnInit(): void {
        this.sub = this.router.params.subscribe(params => {

            this.problemService.getProblem(params['id'])
                .then((res) => {
                    this.problem = res;
                    this.imgSrc = this.problem.image;

                    this.getRecomendation();
                })
                .catch((res) => {
                    console.log(res);
                })
        });
    }

    getRecomendation(): void {
        this.problemService.getRecomendation(this.problem.topic.id)
            .then((res) => {
                this.recomendation = res;
            })
    }

    ngOnDestroy(): void {
        this.sub.unsubscribe();
    }
}