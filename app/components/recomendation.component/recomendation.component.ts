/**
 * Created by calem on 11/13/2016.
 */
import {Component, OnInit, Input} from "@angular/core";
import {Observable, Subscription} from "rxjs";
import {Router, ActivatedRoute} from "@angular/router";
import {AuthService} from "../../services/auth.service";
import {Problem} from "../../model/Problem";
import {ProblemService} from "../../services/problem.service";
import {Subscribable} from "rxjs/Observable";
import {User} from "../../model/User";
import {UserService} from "../../services/user.service";
/**
 * Created by calem on 11/12/2016.
 */
@Component({
    moduleId: module.id,
    selector: 'recomendation',
    templateUrl: 'recomendation.component.html',
    styleUrls: [
        'recomendation.component.css'
    ],
    providers: [
        UserService,
        ProblemService
    ]
})

export class RecomendationComponent{
    @Input() recom: User;

    constructor(
        private router: ActivatedRoute,
        private auth: AuthService,
        private problemService: ProblemService,
        private userService: UserService,
        private route: Router

    ){}

    ngOnInit(): void {
    }


    onClick(): void {
        this.route.navigate(['/profile', this.recom.pk]);
    }

}