"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by calem on 11/13/2016.
 */
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var auth_service_1 = require("../../services/auth.service");
var problem_service_1 = require("../../services/problem.service");
var User_1 = require("../../model/User");
var user_service_1 = require("../../services/user.service");
/**
 * Created by calem on 11/12/2016.
 */
var RecomendationComponent = (function () {
    function RecomendationComponent(router, auth, problemService, userService, route) {
        this.router = router;
        this.auth = auth;
        this.problemService = problemService;
        this.userService = userService;
        this.route = route;
    }
    RecomendationComponent.prototype.ngOnInit = function () {
    };
    RecomendationComponent.prototype.onClick = function () {
        this.route.navigate(['/profile', this.recom.pk]);
    };
    return RecomendationComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", User_1.User)
], RecomendationComponent.prototype, "recom", void 0);
RecomendationComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'recomendation',
        templateUrl: 'recomendation.component.html',
        styleUrls: [
            'recomendation.component.css'
        ],
        providers: [
            user_service_1.UserService,
            problem_service_1.ProblemService
        ]
    }),
    __metadata("design:paramtypes", [router_1.ActivatedRoute,
        auth_service_1.AuthService,
        problem_service_1.ProblemService,
        user_service_1.UserService,
        router_1.Router])
], RecomendationComponent);
exports.RecomendationComponent = RecomendationComponent;
//# sourceMappingURL=recomendation.component.js.map