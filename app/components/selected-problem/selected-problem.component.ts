import {Component, Input, OnInit} from "@angular/core";
import {Problem} from "../../model/Problem";
import {ProblemService} from "../../services/problem.service";
import {Router} from "@angular/router";
import {AuthService} from "../../services/auth.service";
import {User} from "../../model/User";
/**
 * Created by calem on 11/12/2016.
 */
@Component({
    moduleId: module.id,
    selector: 'selected-problem',
    templateUrl: 'selected-problem.component.html',
    styleUrls: [
        'selected-problem.component.css'
    ],
    providers: [
        ProblemService
    ]
})

export class SelectedProblemComponent implements OnInit{
    @Input() problem : Problem;
    private hover: boolean = false;
    private user: User;
    private voted: boolean = false;

    constructor(
        private problemService: ProblemService,
        private router: Router,
        private auth: AuthService
    ) {}

    ngOnInit(): void {
        this.user = this.auth.getUserInfo();
    }

    onMouseEnter():void {
        this.hover=true;
    }

    onMouseLeave():void {
        this.hover=false;
    }

    onSelectedClicked(): void {
        this.router.navigate(['/problem',this.problem.id])
    }

    voteUp(): void {
        let self = this;

        let vote = {
            'user_id': self.user.id,
            'problem_id': self.problem.id,
            'vote': 1
        };

        this.problemService.newProblemVote(vote)
            .then((res) => {
                if(!this.voted) {
                    this.problem.votes_up++;
                } else {
                    this.problem.votes_up--;
                }
                this.voted = true;
            })
    }
}