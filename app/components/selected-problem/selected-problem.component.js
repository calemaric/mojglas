"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var Problem_1 = require("../../model/Problem");
var problem_service_1 = require("../../services/problem.service");
var router_1 = require("@angular/router");
var auth_service_1 = require("../../services/auth.service");
/**
 * Created by calem on 11/12/2016.
 */
var SelectedProblemComponent = (function () {
    function SelectedProblemComponent(problemService, router, auth) {
        this.problemService = problemService;
        this.router = router;
        this.auth = auth;
        this.hover = false;
        this.voted = false;
    }
    SelectedProblemComponent.prototype.ngOnInit = function () {
        this.user = this.auth.getUserInfo();
    };
    SelectedProblemComponent.prototype.onMouseEnter = function () {
        this.hover = true;
    };
    SelectedProblemComponent.prototype.onMouseLeave = function () {
        this.hover = false;
    };
    SelectedProblemComponent.prototype.onSelectedClicked = function () {
        this.router.navigate(['/problem', this.problem.id]);
    };
    SelectedProblemComponent.prototype.voteUp = function () {
        var _this = this;
        var self = this;
        var vote = {
            'user_id': self.user.id,
            'problem_id': self.problem.id,
            'vote': 1
        };
        this.problemService.newProblemVote(vote)
            .then(function (res) {
            if (!_this.voted) {
                _this.problem.votes_up++;
            }
            else {
                _this.problem.votes_up--;
            }
            _this.voted = true;
        });
    };
    return SelectedProblemComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Problem_1.Problem)
], SelectedProblemComponent.prototype, "problem", void 0);
SelectedProblemComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'selected-problem',
        templateUrl: 'selected-problem.component.html',
        styleUrls: [
            'selected-problem.component.css'
        ],
        providers: [
            problem_service_1.ProblemService
        ]
    }),
    __metadata("design:paramtypes", [problem_service_1.ProblemService,
        router_1.Router,
        auth_service_1.AuthService])
], SelectedProblemComponent);
exports.SelectedProblemComponent = SelectedProblemComponent;
//# sourceMappingURL=selected-problem.component.js.map