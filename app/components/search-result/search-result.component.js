"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var problem_service_1 = require("../../services/problem.service");
var auth_service_1 = require("../../services/auth.service");
var router_1 = require("@angular/router");
var Filter_1 = require("../../model/Filter");
var common_1 = require("@angular/common");
var http_1 = require("@angular/http");
/**
 * Created by calem on 11/12/2016.
 */
var SearchResultComponent = (function () {
    function SearchResultComponent(problemService, auth, route, location) {
        this.problemService = problemService;
        this.auth = auth;
        this.route = route;
        this.location = location;
        this.date = '';
        this.topicFilter = [];
        this.min = 0;
        this.filterQuery = {
            1: false,
            2: false,
            3: false,
            4: false,
            5: false,
            6: false,
            7: false,
            8: false,
            9: false,
            10: false,
            11: false,
            12: false,
            13: false,
            14: false,
            15: false,
            16: false
        };
        this.searchObject = new Filter_1.Filter();
        this.searchResults = [];
        this.hover = false;
    }
    SearchResultComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.userInfo = this.auth.getUserInfo();
        this.sub = this.route.queryParams.subscribe(function (params) {
            _this.searchQuery = params['search-query'];
            if (params['filter']) {
                var queryArray = params['filter'].split(encodeURIComponent(','));
                _this.topicFilter.concat(queryArray);
                queryArray.forEach(function (query) {
                    _this.topicFilter.push(query);
                    _this.filterQuery[query] = true;
                });
            }
            _this.searchObject.topic = _this.topicFilter;
            _this.searchObject.search_query = _this.searchQuery;
            _this.problemService.getProblems(_this.searchObject.deserialize())
                .then(function (res) {
                _this.searchResults = res;
            })
                .catch(function (res) {
            });
        });
    };
    SearchResultComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    SearchResultComponent.prototype.filterBy = function (id) {
        var _this = this;
        var inTopics = this.topicFilter.indexOf(id);
        if (inTopics == -1) {
            this.topicFilter.push(id);
        }
        else {
            this.topicFilter.splice(inTopics, 1);
        }
        this.location.go('search?' + this.createQueryUrl().toString());
        this.problemService.getProblems(this.searchObject.deserialize())
            .then(function (res) {
            _this.searchResults = res;
        });
    };
    SearchResultComponent.prototype.createQueryUrl = function () {
        var params = new http_1.URLSearchParams();
        params.set('search-query', this.searchQuery);
        var filterString = '';
        for (var i = 0; i < this.topicFilter.length; i++) {
            filterString += this.topicFilter[i];
            if (i < this.topicFilter.length - 1) {
                filterString += encodeURIComponent(',');
            }
        }
        params.append('filter', filterString);
        return params;
    };
    return SearchResultComponent;
}());
SearchResultComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'search-result',
        templateUrl: 'search-result.component.html',
        styleUrls: [
            'search-result.component.css'
        ],
        providers: [
            problem_service_1.ProblemService
        ]
    }),
    __metadata("design:paramtypes", [problem_service_1.ProblemService,
        auth_service_1.AuthService,
        router_1.ActivatedRoute,
        common_1.Location])
], SearchResultComponent);
exports.SearchResultComponent = SearchResultComponent;
//# sourceMappingURL=search-result.component.js.map