import {Component, Input, OnInit} from "@angular/core";
import {Problem} from "../../model/Problem";
import {ProblemService} from "../../services/problem.service";
import {AuthService} from "../../services/auth.service";
import {User} from "../../model/User";
import {Router, ActivatedRoute, Route, UrlSerializer} from "@angular/router";
import {Filter} from "../../model/Filter";
import {Location} from "@angular/common";
import {URLSearchParams} from "@angular/http";
import {UrlResolver} from "@angular/compiler";
import {forEach} from "@angular/router/src/utils/collection";
/**
 * Created by calem on 11/12/2016.
 */

@Component({
    moduleId: module.id,
    selector: 'search-result',
    templateUrl: 'search-result.component.html',
    styleUrls: [
        'search-result.component.css'
    ],
    providers: [
        ProblemService
    ]
})

export class SearchResultComponent implements OnInit {
    private searchQuery: string;
    private date: string = '';
    private topicFilter: number[] = [];
    private min: number = 0;
    private filterQuery: any = {
        1: false,
        2: false,
        3: false,
        4: false,
        5: false,
        6: false,
        7: false,
        8: false,
        9: false,
        10: false,
        11: false,
        12: false,
        13: false,
        14: false,
        15: false,
        16: false
    };

    private searchObject: Filter = new Filter();

    private searchResults: Problem[] = [];

    private userInfo: User;

    private hover: boolean = false;
    private sub: any;

    constructor(
        private problemService: ProblemService,
        private auth: AuthService,
        private route: ActivatedRoute,
        private location: Location
    ) {}

    ngOnInit(): void {
        this.userInfo = this.auth.getUserInfo();

        this.sub = this.route.queryParams.subscribe(params => {
            this.searchQuery = params['search-query'];

            if(params['filter']) {
                let queryArray = params['filter'].split(encodeURIComponent(','));
                this.topicFilter.concat(queryArray);

                queryArray.forEach(query => {
                    this.topicFilter.push(query);

                    this.filterQuery[query] = true;
                });
            }

            this.searchObject.topic = this.topicFilter;

            this.searchObject.search_query = this.searchQuery;
            this.problemService.getProblems(this.searchObject.deserialize())
                .then((res) => {
                    this.searchResults = res;
                })
                .catch((res) => {
                })
        });

    }

    ngOnDestroy(): void {
        this.sub.unsubscribe();
    }

    filterBy(id): void {
        let inTopics = this.topicFilter.indexOf(id);

        if(inTopics == -1) {
            this.topicFilter.push(id);
        } else {
            this.topicFilter.splice(inTopics,1);
        }

        this.location.go('search?'+this.createQueryUrl().toString());

        this.problemService.getProblems(this.searchObject.deserialize())
            .then(
                (res) => {
                    this.searchResults = res;
                }
            )
    }

    createQueryUrl(): URLSearchParams {
        let params = new URLSearchParams();
        params.set('search-query',this.searchQuery);

        let filterString = '';

        for(let i=0;i<this.topicFilter.length;i++)  {
            filterString += this.topicFilter[i];

            if(i < this.topicFilter.length - 1) {
                filterString += encodeURIComponent(',');
            }
        }

        params.append('filter', filterString);
        return params;
    }

}