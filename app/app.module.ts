/**
 * Created by calem on 11/9/2016.
 */
import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {FormsModule} from "@angular/forms";
import {AppRoutingModule} from "./app-routing.module";
import {HttpModule} from "@angular/http";
import {LoggedInGuard} from "./services/LoggedInGuard";
import {AuthService} from "./services/auth.service";
import {AppComponent} from "./components/app.component";
import {LoginPageComponent} from "./components/login-page/login-page.component";
import {RegisterFormComponent} from "./components/login-page/register-form/register-form.component";
import {HomePageComponent} from "./components/home-page/home-page.component";
import {ProfilePageComponent} from "./components/profile-page/profile-page.component";
import {SearchResultComponent} from "./components/search-result/search-result.component";
import {ProblemItemComponent} from "./components/problem-item/problem-item.component";
import {Problem} from "./model/Problem";
import {ProblemPageComponent} from "./components/problem-page/problem-page.component";
import {SelectedProblemComponent} from "./components/selected-problem/selected-problem.component";
import {SearchProblemComponent} from "./components/search-problem/search-problem.component";
import {RecomendationComponent} from "./components/recomendation.component/recomendation.component";


@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        AppRoutingModule
    ],
    declarations: [
        AppComponent,
        LoginPageComponent,
        RegisterFormComponent,
        HomePageComponent,
        ProblemItemComponent,
        ProfilePageComponent,
        SearchResultComponent,
        ProblemPageComponent,
        SelectedProblemComponent,
        SearchProblemComponent,
        RecomendationComponent
    ],
    bootstrap: [
        AppComponent
    ],
    providers: [
        AuthService,
        LoggedInGuard
    ]
})

export class AppModule {}


