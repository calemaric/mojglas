/**
 * Created by calem on 11/11/2016.
 */
import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import '../model/constants';
import {httpUrl, jsonHeaders, citizenUrl, parlamentUrl} from "../model/constants";
import 'rxjs/add/operator/toPromise';
import {Problem} from "../model/Problem";
import {User} from "../model/User";
import * as moment from 'moment/moment';


@Injectable()
export class ProblemService {

    constructor(
        private http: Http
    ) {}

    getProblems(filter: any): Promise<Problem[]> {
        filter.date = moment(filter.date).format('YYYY-MM-DD');
        return this.http.post(httpUrl + citizenUrl + 'problem/filter',filter)
            .toPromise()
            .then((res) => {
                return res.json();
            });
    }

    getProblem(id: number) : Promise<any> {
        return this.http.get(httpUrl + citizenUrl + 'problem/'+id)
            .toPromise()
            .then((res) => {
                return res.json();
            })
    }

    newProblem(values) : Promise<Problem> {
        return this.http.post(httpUrl + citizenUrl + 'problem/new/', values, jsonHeaders)
            .toPromise()
            .then((res) => {
                console.log(res.json());
                return res.json();
            })
    }

    newProblemVote(values) : Promise<boolean> {
        return this.http.post(httpUrl + citizenUrl + 'problem/vote', values, jsonHeaders)
            .toPromise()
            .then((res) => {
                return true;
            })
    }

    getRecomendation(values) : Promise<User[]> {
        return this.http.get(httpUrl + parlamentUrl + 'member/recommended/' + values, jsonHeaders)
            .toPromise()
            .then((res) => {
                console.log(res);
                return res.json();
            })
    }}