/**
 * Created by calem on 11/12/2016.
 */
import {Injectable} from "@angular/core";
import {User} from "../model/User";
import {URLSearchParams, Http} from "@angular/http";

import "../model/constants";
import {httpUrl, citizenUrl, parlamentUrl} from "../model/constants";
import 'rxjs/add/operator/toPromise';



@Injectable()
export class UserService {

    constructor(
        private http: Http
    ) {}

    getIndividualUser(id: number): Promise<User> {
        return this.http.get(httpUrl + parlamentUrl + 'member/' + id)
            .toPromise()
            .then((res) => {
                return res.json();
            })
    }


}