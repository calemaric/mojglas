/**
 * Created by calem on 11/10/2016.
 */
import {Injectable} from "@angular/core";
import {Http, Headers} from "@angular/http";

import 'rxjs/add/operator/toPromise';
import '../model/constants';
import {httpUrl, authUrl, jsonHeaders, citizenUrl} from "../model/constants";
import {User} from "../model/User";

@Injectable()
export class AuthService {
    private loggedIn: boolean = false;
    private userInfo: User;

    constructor(
        private http: Http
    ) {
        this.loggedIn = !!localStorage.getItem('username') && !!localStorage.getItem('password');

        if(this.loggedIn) {
            let creds = {
                username: localStorage.getItem('username'),
                password: localStorage.getItem('password')
            }
            this.login(creds)
                .then(() => {});
        }
    }

    isLoggedIn() : boolean {
        return this.loggedIn;
    }

    getUserInfo(): any {
        return this.userInfo;
    }

    login(loginCreds) : Promise<User> {
        return this.http.post(httpUrl + citizenUrl + 'login/',loginCreds,jsonHeaders)
            .toPromise()
            .then((res:any) => {
                if (res.status == '200') {
                    localStorage.setItem('username', loginCreds.username);
                    localStorage.setItem('password', loginCreds.password);
                    this.loggedIn = true;
                    this.userInfo = res.json();

                    return res.json();
                }
            })
            .catch(this.handleError)
    }

    register(userCreds): Promise<any> {
        return this.http.post(httpUrl + citizenUrl + 'register/', userCreds, jsonHeaders)
            .toPromise()
            .then((res) => {
                return res.status;
            })
            .catch(this.handleError);
    }

    logout(): void {
        localStorage.removeItem('auth-token');
        this.loggedIn = false;
    }

    handleError(err: any): Promise<void> {
        return Promise.reject(err);
    }
}