"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by calem on 11/10/2016.
 */
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
require("../model/constants");
var constants_1 = require("../model/constants");
var AuthService = (function () {
    function AuthService(http) {
        this.http = http;
        this.loggedIn = false;
        this.loggedIn = !!localStorage.getItem('username') && !!localStorage.getItem('password');
        if (this.loggedIn) {
            var creds = {
                username: localStorage.getItem('username'),
                password: localStorage.getItem('password')
            };
            this.login(creds)
                .then(function () { });
        }
    }
    AuthService.prototype.isLoggedIn = function () {
        return this.loggedIn;
    };
    AuthService.prototype.getUserInfo = function () {
        return this.userInfo;
    };
    AuthService.prototype.login = function (loginCreds) {
        var _this = this;
        return this.http.post(constants_1.httpUrl + constants_1.citizenUrl + 'login/', loginCreds, constants_1.jsonHeaders)
            .toPromise()
            .then(function (res) {
            if (res.status == '200') {
                localStorage.setItem('username', loginCreds.username);
                localStorage.setItem('password', loginCreds.password);
                _this.loggedIn = true;
                _this.userInfo = res.json();
                return res.json();
            }
        })
            .catch(this.handleError);
    };
    AuthService.prototype.register = function (userCreds) {
        return this.http.post(constants_1.httpUrl + constants_1.citizenUrl + 'register/', userCreds, constants_1.jsonHeaders)
            .toPromise()
            .then(function (res) {
            return res.status;
        })
            .catch(this.handleError);
    };
    AuthService.prototype.logout = function () {
        localStorage.removeItem('auth-token');
        this.loggedIn = false;
    };
    AuthService.prototype.handleError = function (err) {
        return Promise.reject(err);
    };
    return AuthService;
}());
AuthService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], AuthService);
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map