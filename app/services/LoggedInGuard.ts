/**
 * Created by calem on 11/11/2016.
 */
import {Injectable} from "@angular/core";
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from "@angular/router";
import {Observable} from "rxjs";
import {AuthService} from "./auth.service";


@Injectable()
export class LoggedInGuard implements CanActivate {
    constructor(
        private user: AuthService,
        private router: Router
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>|Promise<boolean>|boolean {
        if(!this.user.isLoggedIn()) {
            this.router.navigate(['/login']);
        }
        return this.user.isLoggedIn();
    }

}