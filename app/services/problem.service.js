"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by calem on 11/11/2016.
 */
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("../model/constants");
var constants_1 = require("../model/constants");
require("rxjs/add/operator/toPromise");
var moment = require("moment/moment");
var ProblemService = (function () {
    function ProblemService(http) {
        this.http = http;
    }
    ProblemService.prototype.getProblems = function (filter) {
        filter.date = moment(filter.date).format('YYYY-MM-DD');
        return this.http.post(constants_1.httpUrl + constants_1.citizenUrl + 'problem/filter', filter)
            .toPromise()
            .then(function (res) {
            return res.json();
        });
    };
    ProblemService.prototype.getProblem = function (id) {
        return this.http.get(constants_1.httpUrl + constants_1.citizenUrl + 'problem/' + id)
            .toPromise()
            .then(function (res) {
            return res.json();
        });
    };
    ProblemService.prototype.newProblem = function (values) {
        return this.http.post(constants_1.httpUrl + constants_1.citizenUrl + 'problem/new/', values, constants_1.jsonHeaders)
            .toPromise()
            .then(function (res) {
            console.log(res.json());
            return res.json();
        });
    };
    ProblemService.prototype.newProblemVote = function (values) {
        return this.http.post(constants_1.httpUrl + constants_1.citizenUrl + 'problem/vote', values, constants_1.jsonHeaders)
            .toPromise()
            .then(function (res) {
            return true;
        });
    };
    ProblemService.prototype.getRecomendation = function (values) {
        return this.http.get(constants_1.httpUrl + constants_1.parlamentUrl + 'member/recommended/' + values, constants_1.jsonHeaders)
            .toPromise()
            .then(function (res) {
            console.log(res);
            return res.json();
        });
    };
    return ProblemService;
}());
ProblemService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], ProblemService);
exports.ProblemService = ProblemService;
//# sourceMappingURL=problem.service.js.map