import {User} from "./User";
/**
 * Created by calem on 11/12/2016.
 */
export class Problem {
    private _id: number;
    private _title: string;
    private _description: string;
    private _votes_up: number;
    private _votes_down: number;
    private _date: Date;
    private _user: User;
    private _topic: string;
    private _image: string;

    constructor() {

    }


    constructor(id: number, title: string, description: string, votes_up: number, votes_down: number, user: User, topic: string, image: string, date: Date) {
        this._id = id;
        this._title = title;
        this._description = description;
        this._votes_up = votes_up;
        this._votes_down = votes_down;
        this._user = user;
        this._topic = topic;
        this._image = image;
        this._date = date;
    }


    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get title(): string {
        return this._title;
    }

    set title(value: string) {
        this._title = value;
    }

    get description(): string {
        return this._description;
    }

    set description(value: string) {
        this._description = value;
    }

    get votes_up(): number {
        return this._votes_up;
    }

    set votes_up(value: number) {
        this._votes_up = value;
    }

    get votes_down(): number {
        return this._votes_down;
    }

    set votes_down(value: number) {
        this._votes_down = value;
    }

    get user(): User {
        return this._user;
    }

    set user(value: User) {
        this._user = value;
    }

    get topic(): string {
        return this._topic;
    }

    set topic(value: string) {
        this._topic = value;
    }


    get image(): string {
        return this._image;
    }

    set image(value: string) {
        this._image = value;
    }


    get date(): Date {
        return this._date;
    }

    set date(value: Date) {
        this._date = value;
    }
}