import {Problem} from "./Problem";
/**
 * Created by calem on 11/12/2016.
 */
export class User {
    private _id: number;
    private _username: string;
    private _first_name: string;
    private _last_name: string;
    private _email: string;
    private _image: string;
    private _type: number;
    private _about: string;
    private _municipality: string;
    private _problems: Problem[];

    constructor() {}

    constructor(
        id: number,
        username: string,
        first_name: string,
        last_name: string,
        email: string,
        image: string,
        type: number,
        about: string,
        municipality: string,
        problems: Problem[]
    ) {
        this._id = id;
        this._username = username;
        this._first_name = first_name;
        this._last_name = last_name;
        this._email = email;
        this._image = image;
        this._type = type;
        this._municipality = municipality;
        this._about = about;
        this._problems = problems;
    }


    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get username(): string {
        return this._username;
    }

    set username(value: string) {
        this._username = value;
    }

    get first_name(): string {
        return this._first_name;
    }

    set first_name(value: string) {
        this._first_name = value;
    }

    get last_name(): string {
        return this._last_name;
    }

    set last_name(value: string) {
        this._last_name = value;
    }

    get email(): string {
        return this._email;
    }

    set email(value: string) {
        this._email = value;
    }

    get image(): string {
        return this._image;
    }

    set image(value: string) {
        this._image = value;
    }


    get type(): number {
        return this._type;
    }

    set type(value: number) {
        this._type = value;
    }

    get about(): string {
        return this._about;
    }

    set about(value: string) {
        this._about = value;
    }

    get municipality(): string {
        return this._municipality;
    }

    set municipality(value: string) {
        this._municipality = value;
    }

    convertType() {
        switch (this._type) {
            case 0:
                return 'civil';
                break;
            case 1:
                return 'organization';
                break;
            case 2:
                return 'journalist';
                break;
            default:
                return 'pravno lice';
                break;
        }
    }

    get problems(): Problem[] {
        return this._problems;
    }

    set problems(value: Array) {
        this._problems = value;
    }
}