"use strict";
var Filter = (function () {
    function Filter() {
        this._date = new Date();
        this._min = 0;
        this._number_of = 0;
        this._order_by = '';
        this._search_query = '';
        this._topic = [];
    }
    Object.defineProperty(Filter.prototype, "date", {
        get: function () {
            return this._date;
        },
        set: function (value) {
            this._date = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Filter.prototype, "topic", {
        get: function () {
            return this._topic;
        },
        set: function (value) {
            this._topic = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Filter.prototype, "min", {
        get: function () {
            return this._min;
        },
        set: function (value) {
            this._min = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Filter.prototype, "search_query", {
        get: function () {
            return this._search_query;
        },
        set: function (value) {
            this._search_query = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Filter.prototype, "order_by", {
        get: function () {
            return this._order_by;
        },
        set: function (value) {
            this._order_by = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Filter.prototype, "number_of", {
        get: function () {
            return this._number_of;
        },
        set: function (value) {
            this._number_of = value;
        },
        enumerable: true,
        configurable: true
    });
    Filter.prototype.deserialize = function () {
        return {
            date: this._date || '',
            topic: this._topic || [],
            min: this._min || 0,
            search_query: this._search_query || '',
            order_by: this._order_by || '',
            number_of: this._number_of || 0
        };
    };
    return Filter;
}());
exports.Filter = Filter;
//# sourceMappingURL=Filter.js.map