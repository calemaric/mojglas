import {Headers} from "@angular/http";
/**
 * Created by calem on 11/11/2016.
 */
export const httpUrl = 'http://localhost:8000/';
export const authUrl = 'auth/';
export const citizenUrl = 'citizen/';
export const parlamentUrl = 'parliament/';

export const jsonHeaders = new Headers({
    'Content-Type': 'application/json'
});