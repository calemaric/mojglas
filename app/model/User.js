"use strict";
/**
 * Created by calem on 11/12/2016.
 */
var User = (function () {
    function User() {
    }
    Object.defineProperty(User.prototype, "id", {
        get: function () {
            return this._id;
        },
        set: function (value) {
            this._id = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "username", {
        get: function () {
            return this._username;
        },
        set: function (value) {
            this._username = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "first_name", {
        get: function () {
            return this._first_name;
        },
        set: function (value) {
            this._first_name = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "last_name", {
        get: function () {
            return this._last_name;
        },
        set: function (value) {
            this._last_name = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "email", {
        get: function () {
            return this._email;
        },
        set: function (value) {
            this._email = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "image", {
        get: function () {
            return this._image;
        },
        set: function (value) {
            this._image = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "type", {
        get: function () {
            return this._type;
        },
        set: function (value) {
            this._type = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "about", {
        get: function () {
            return this._about;
        },
        set: function (value) {
            this._about = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "municipality", {
        get: function () {
            return this._municipality;
        },
        set: function (value) {
            this._municipality = value;
        },
        enumerable: true,
        configurable: true
    });
    User.prototype.convertType = function () {
        switch (this._type) {
            case 0:
                return 'civil';
                break;
            case 1:
                return 'organization';
                break;
            case 2:
                return 'journalist';
                break;
            default:
                return 'pravno lice';
                break;
        }
    };
    Object.defineProperty(User.prototype, "problems", {
        get: function () {
            return this._problems;
        },
        set: function (value) {
            this._problems = value;
        },
        enumerable: true,
        configurable: true
    });
    return User;
}());
exports.User = User;
//# sourceMappingURL=User.js.map