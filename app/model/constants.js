"use strict";
var http_1 = require("@angular/http");
/**
 * Created by calem on 11/11/2016.
 */
exports.httpUrl = 'http://localhost:8000/';
exports.authUrl = 'auth/';
exports.citizenUrl = 'citizen/';
exports.parlamentUrl = 'parliament/';
exports.jsonHeaders = new http_1.Headers({
    'Content-Type': 'application/json'
});
//# sourceMappingURL=constants.js.map