"use strict";
/**
 * Created by calem on 11/12/2016.
 */
var Problem = (function () {
    function Problem() {
    }
    Object.defineProperty(Problem.prototype, "id", {
        get: function () {
            return this._id;
        },
        set: function (value) {
            this._id = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Problem.prototype, "title", {
        get: function () {
            return this._title;
        },
        set: function (value) {
            this._title = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Problem.prototype, "description", {
        get: function () {
            return this._description;
        },
        set: function (value) {
            this._description = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Problem.prototype, "votes_up", {
        get: function () {
            return this._votes_up;
        },
        set: function (value) {
            this._votes_up = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Problem.prototype, "votes_down", {
        get: function () {
            return this._votes_down;
        },
        set: function (value) {
            this._votes_down = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Problem.prototype, "user", {
        get: function () {
            return this._user;
        },
        set: function (value) {
            this._user = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Problem.prototype, "topic", {
        get: function () {
            return this._topic;
        },
        set: function (value) {
            this._topic = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Problem.prototype, "image", {
        get: function () {
            return this._image;
        },
        set: function (value) {
            this._image = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Problem.prototype, "date", {
        get: function () {
            return this._date;
        },
        set: function (value) {
            this._date = value;
        },
        enumerable: true,
        configurable: true
    });
    return Problem;
}());
exports.Problem = Problem;
//# sourceMappingURL=Problem.js.map