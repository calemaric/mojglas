export class Filter {
    private _date: Date;
    private _topic: number[];
    private _min: number;
    private _search_query: string;
    private _order_by: string;
    private _number_of: number;

    constructor() {
        this._date = new Date();
        this._min = 0;
        this._number_of = 0;
        this._order_by = '';
        this._search_query = '';
        this._topic = [];
    }


    get date(): Date {
        return this._date;
    }

    set date(value: Date) {
        this._date = value;
    }

    get topic(): number[] {
        return this._topic;
    }

    set topic(value: number[]) {
        this._topic = value;
    }

    get min(): number {
        return this._min;
    }

    set min(value: number) {
        this._min = value;
    }

    get search_query(): string {
        return this._search_query;
    }

    set search_query(value: string) {
        this._search_query = value;
    }

    get order_by(): string {
        return this._order_by;
    }

    set order_by(value: string) {
        this._order_by = value;
    }

    get number_of(): number {
        return this._number_of;
    }

    set number_of(value: number) {
        this._number_of = value;
    }

    deserialize(): any {
        return {
            date : this._date || '',
            topic: this._topic || [],
            min: this._min || 0,
            search_query: this._search_query || '',
            order_by: this._order_by || '',
            number_of: this._number_of || 0
        }
    }
}